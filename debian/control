Source: libcommons-dbcp-java
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Emmanuel Bourg <ebourg@apache.org>
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 junit,
 libcommons-pool-java,
 libgeronimo-jta-1.2-spec-java,
 libtomcat10-java,
 maven-debian-helper (>= 1.4)
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/java-team/libcommons-dbcp-java.git
Vcs-Browser: https://salsa.debian.org/java-team/libcommons-dbcp-java
Homepage: http://commons.apache.org/dbcp/

Package: libcommons-dbcp-java
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Suggests: ${maven:OptionalDepends}
Description: Database Connection Pooling Services
 The DBCP package provides database connection pooling services. The
 following features are supported
 .
  * DataSource and Driver interfaces to the pool.
  * Support for arbitrary sources of the underlying Connections.
  * Integration with arbitrary org.apache.commons.pool.ObjectPool
    implementations.
  * Support for Connection validation, expiration, etc.
  * Support for PreparedStatement pooling.
  * XML configuration.
 .
 This is a part of the Apache Commons Project.
